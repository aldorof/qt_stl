#include "Loader.h"
#include "Mesh.h"

//-------------------------------------------------------------------//
Loader::Loader(QObject* parent, const QString& filename): QThread(parent), 
        fFileName(filename) {
}
//-------------------------------------------------------------------//
void Loader::run() {
    emit got_mesh( Mesh::LoadSTL(fFileName) );
}
//-------------------------------------------------------------------//
