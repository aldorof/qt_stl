#include "GLMesh.h"
#include "Mesh.h"

//-------------------------------------------------------------------//
GLMesh::GLMesh(const Mesh* const mesh) : 
        fVert(QOpenGLBuffer::VertexBuffer), fIndx(QOpenGLBuffer::IndexBuffer) {

    fVert.create();
    fIndx.create();

    fVert.setUsagePattern(QOpenGLBuffer::StaticDraw);
    fIndx.setUsagePattern(QOpenGLBuffer::StaticDraw);

    fVert.bind();
    fVert.allocate( mesh->fVertices.size() * 3 * sizeof(float));
    for (auto it = mesh->fVertices.cbegin(); it != mesh->fVertices.cend(); ++it){
        // we'll need to convert integer to float here.
        float buff[3] { (float) it->second.fX/mesh->fConv,
                        (float) it->second.fY/mesh->fConv,
                        (float) it->second.fZ/mesh->fConv };

        fVert.write((3 * (it->first - 1) )*sizeof(float), buff, 3 * sizeof(float));
    }
    fVert.release();

    fIndx.bind();
    fIndx.allocate( mesh->fFaces.size() * 3 * sizeof(uint32_t));
    for (auto it = mesh->fFaces.cbegin(); it != mesh->fFaces.cend(); ++it){
        // converting size_t to uint32_t here....
        uint32_t buff[3] {  (uint32_t) it->second.fVert[0] - 1,
                            (uint32_t) it->second.fVert[1] - 1,
                            (uint32_t) it->second.fVert[2] - 1 };
        fIndx.write((3 * (it->first - 1) )*sizeof(uint32_t), buff, 3 * sizeof(uint32_t));
    }
    fIndx.release();
}
//-------------------------------------------------------------------//
