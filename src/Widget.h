#ifndef _WIDGET_H
#define _WIDGET_H


#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>

class GLMesh;
class Mesh;

class Widget : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT

  public:
    Widget(QWidget* parent=0);
    ~Widget();

    void initializeGL();
    void paintGL();
   

  public slots:
    void LoadMesh(Mesh* m);

  protected:
    void mousePressEvent  (QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent   (QMouseEvent* event);

  private:
    void DrawMesh();

    QMatrix4x4 TransformMatrix() const;
    QMatrix4x4      ViewMatrix() const;

  private:
    QOpenGLShaderProgram fMeshShader;
    GLMesh* fMesh;
    QVector3D fCenter;
    float fScale;
    float fTilt;
    float fYaw;
    QPoint fMousePos;
};

#endif // _WIDGET_H
