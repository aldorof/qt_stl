#ifndef _WINDOW_H
#define _WINDOW_H

#include <QMainWindow>
#include <QActionGroup>

class Widget;
class Window : public QMainWindow {
    Q_OBJECT
  public:
    explicit Window(QWidget* parent=0);
    void LoadSTL(const QString& filename);

  public slots:
    void onOpen();

  private:
    Widget* fWidget;

    QAction* const fOpenAction;
};

#endif // _WINDOW_H
