#include <QMenuBar>
#include <QFileDialog>

#include "Window.h"
#include "Widget.h"
#include "Loader.h"

//-------------------------------------------------------------------//
Window::Window(QWidget *parent): QMainWindow(parent), fOpenAction(new QAction("Open", this)) {
    setWindowTitle("Sim3Demo");

    fWidget = new Widget(this);
    setCentralWidget(fWidget);

    fOpenAction->setShortcut(QKeySequence::Open);
    QObject::connect(fOpenAction, &QAction::triggered, this, &Window::onOpen);

    auto file_menu = menuBar()->addMenu("File");
    file_menu->addAction(fOpenAction);

    resize(600, 1000);
}
//-------------------------------------------------------------------//
void Window::LoadSTL(const QString &filename) {

    Loader* loader = new Loader(this, filename);

    connect(loader, SIGNAL(got_mesh(Mesh*)),
            fWidget, SLOT(LoadMesh(Mesh*)));

    connect(loader, SIGNAL(finished()),
            loader, SLOT(deleteLater()));

    loader->start();
}
//-------------------------------------------------------------------//
void Window::onOpen(){
    QString filename = QFileDialog::getOpenFileName( this, 
        "Load STL file", QString(), "STL files (*.stl, *.STL)");
    if (!filename.isNull()) {
        LoadSTL(filename);
    } 
}
//-------------------------------------------------------------------//
