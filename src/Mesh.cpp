#include <iostream>     // for cout
#include <algorithm>

#include "Mesh.h"

//-------------------------------------------------------------------//
MeshVertex::MeshVertex(long long x, long long y, long long z): fX(x), fY(y), fZ(z) {
}
//-------------------------------------------------------------------//
bool MeshVertex::operator ==(const MeshVertex& vert) const{
    return fX == vert.fX && fY == vert.fY && fZ == vert.fZ;
}
//===================================================================//
MeshEdge::MeshEdge(size_t from, size_t to): fVert{from, to} {
}
//===================================================================//
MeshFace::MeshFace(size_t vrtx1, size_t vrtx2, size_t vrtx3): fVert{vrtx1, vrtx2, vrtx3} {
}
//===================================================================//
Mesh::Mesh(unsigned long long mm_to_int) : fConv(mm_to_int),
        fNavFace(1), fNavEdge(1), fNavVertex(1) {
}
//-------------------------------------------------------------------//
Mesh* Mesh::LoadSTL(const QString& filename) {
    std::cout << "...Loading STL from " << filename.toUtf8().constData() << std::endl;
    Mesh *result = new Mesh();
    if (result == NULL)
        return result;

    // For now, let's load Binary STL into the file
    if ( result->LoadBinarySTL(filename.toUtf8().constData()) ) {
        std::cout << "...Loaded binary STL from " << filename.toUtf8().constData() << std::endl;
    } else {
        // Let's try to load ASCII
        if ( result->LoadTextSTL(filename.toUtf8().constData()) ) {
            std::cout << "...Loaded ASCII STL from " << filename.toUtf8().constData() << std::endl;
        } else {
            std::cout << "...Error loading STL from " << filename.toUtf8().constData() << std::endl;
        }
    }
    // Let's add problematic faces calculation here
    size_t nof_problem_faces = result->CalculateProblematicFaces();
    std::cout << "...Problematic faces = " << nof_problem_faces << std::endl;

    return result;
}
//-------------------------------------------------------------------//
bool Mesh::LoadBinarySTL(std::string filename){
    // TODO in principle we want to clear old data here somewhere
    FILE* f = fopen(filename.c_str(), "rb");
    if ( f == NULL ) {
        return false;
    }
    fseek(f, 0L, SEEK_END);
    size_t file_size = ftell(f);
    // Header is 80 bytes, each face is 50bytes.
    if (file_size < 80) {
        // No reason for continuing, because we don't have even a header
        fclose(f);
        return false;
    }
    rewind(f); // back to the beginning of the file
    size_t face_count = (file_size - 80 - sizeof(uint32_t)) / 50;
    if (face_count < 1) {
        // No reason for continuing because we don't have any faces
        fclose(f);
        return false;
    }
    // We're just ignoring the header
    fseek(f, 80, SEEK_CUR);
    uint32_t reported_face_count;
    if (fread(&reported_face_count, sizeof(uint32_t), 1, f) != 1) {
        fclose(f);
        return false;
    }
    // Check if the file is corrupted
    if (reported_face_count != face_count) {
        fclose(f);
        return false;
    }
    // OK, read the faces in sequential order and add them to mesh
    // right now we will not test if addFace function was successfull or not
    char buffer[50];
    for (size_t ii=0; ii < face_count; ++ii) {
        if (fread(buffer, 50, 1, f) != 1) {
            fclose(f);
            return false;
        }
        float *v= ((float*)buffer)+3;
        MeshVertex v0 = MeshVertex( 
            (long long) (v[0]*fConv), 
            (long long) (v[1]*fConv), 
            (long long) (v[2]*fConv) );
        MeshVertex v1 = MeshVertex( 
            (long long) (v[3]*fConv), 
            (long long) (v[4]*fConv), 
            (long long) (v[5]*fConv) );
        MeshVertex v2 = MeshVertex( 
            (long long) (v[6]*fConv), 
            (long long) (v[7]*fConv), 
            (long long) (v[8]*fConv) );
        AddFace(v0, v1, v2);
    }
    fclose(f);
    return true;
}
//-------------------------------------------------------------------//
bool Mesh::LoadTextSTL(std::string filename){
    /**
     * TODO: Implement this
     */
    return false;
}
//-------------------------------------------------------------------//
size_t Mesh::CalculateProblematicFaces(){
    size_t result = 0;
    std::unordered_map <size_t, unsigned long> problematic_faces;
    // The problematic will be faces, involved with verticies that are not pointing back to this vertice
    for (auto it = fVertices.cbegin(); it != fVertices.cend(); ++it){
        /*
        if ( (it->second.fFromVrt.size() % 2) ) {
            for (auto it_face = it->second.fFaces.cbegin(); it_face != it->second.fFaces.cend(); ++it_face ) {
                problematic_faces[(*it_face)] ++;
            }
        }
        */
    }
    result = problematic_faces.size();
    // std::cout << "There are " << result << " problematic faces" << std::endl;
    /* I think this function will fit all 3 cases
     *  * If all 3 verticies are with errors, this will correspond to duplicate or flipped triangles
     *  * if 1 or 2 vertices are with errors, this will correspond to triangle with no neighbour
     */
    return result;
}
//-------------------------------------------------------------------//
size_t Mesh::AddFace(MeshVertex &vrtx0, MeshVertex &vrtx1, MeshVertex &vrtx2){
    size_t vrtx_idx0 = AddVertex(vrtx0);
    size_t vrtx_idx1 = AddVertex(vrtx1);
    size_t vrtx_idx2 = AddVertex(vrtx2);

    if ( (vrtx_idx0 == vrtx_idx1 ) || (vrtx_idx0 == vrtx_idx2 ) || (vrtx_idx1 == vrtx_idx2 ) ) {
        return 0; // NoFace
    }

    size_t result = fNavFace;
    fNavFace++;

    fFaces.emplace(result, MeshFace(vrtx_idx0, vrtx_idx1, vrtx_idx2) );

    // We want to update connections
    // Let's find all 3 vertices 
    auto vrtx0_it = fVertices.find(vrtx_idx0); // it will never fail :)
    auto vrtx1_it = fVertices.find(vrtx_idx1);
    auto vrtx2_it = fVertices.find(vrtx_idx2);

    vrtx0_it->second.fFromVrt.push_back(vrtx1_it->first);
    vrtx0_it->second.fFaces.push_back( result );

    vrtx1_it->second.fFromVrt.push_back(vrtx2_it->first);
    vrtx1_it->second.fFaces.push_back( result );

    vrtx2_it->second.fFromVrt.push_back(vrtx0_it->first);
    vrtx2_it->second.fFaces.push_back( result );

    return result;
}
//-------------------------------------------------------------------//
size_t Mesh::AddVertex(MeshVertex &vert) {
    // Let's see if the vertex is already there
    auto it = fVertReverse.find(vert);
    if (it != fVertReverse.end()) {
        // We found the vertex point
        return it->second;
    }
    // We'll need to generate new index for this vertex
    size_t result = fNavVertex;
    fNavVertex++;

    fVertices.emplace(result, vert);
    fVertReverse.emplace(vert, result);

    // Recalculate the bounding box
    if (fVertices.size() == 1) {
        // This was the first point added
        fMinX = fMaxX = vert.fX;
        fMinY = fMaxY = vert.fY;
        fMinZ = fMaxZ = vert.fZ;
    } else {
        if ( vert.fX > fMaxX ) fMaxX = vert.fX;
        if ( vert.fY > fMaxY ) fMaxY = vert.fY;
        if ( vert.fZ > fMaxZ ) fMaxZ = vert.fZ;
        if ( vert.fX < fMinX ) fMinX = vert.fX;
        if ( vert.fY < fMinY ) fMinY = vert.fY;
        if ( vert.fZ < fMinZ ) fMinZ = vert.fZ;
    }
    return result;
}
//-------------------------------------------------------------------//
