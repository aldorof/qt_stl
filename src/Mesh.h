#ifndef _MESH_H
#define _MESH_H

#include <QString>

#include <unordered_map>
#include <vector>
//-------------------------------------------------------------------//
class MeshVertex {
  public:
    MeshVertex();
    MeshVertex(long long x, long long y, long long z);

    /**
     * Needed for reversed map
     */
    bool operator ==(const MeshVertex& vert) const;

  public:
    long long fX, fY, fZ;
  /**
   * These will keep track for connections
   */
  public:
    std::vector<size_t> fFromVrt; // indicies of other verticies that point to this vertex
    std::vector<size_t> fFaces;   // Participant faces
    
};
//-------------------------------------------------------------------//
/**
 * This is used as a hash function in reverse vertex maps
 */
struct MeshVertex_hash {
    size_t operator() (const MeshVertex &vert) const {
        return vert.fX ^ (vert.fY << 10) ^ (vert.fZ << 20);
    }
};
//-------------------------------------------------------------------//
class MeshEdge {
  public:
    MeshEdge();
    MeshEdge(size_t from, size_t to);
  public:
    // These are vertex indicies
    size_t fVert[2];
};
//-------------------------------------------------------------------//
class MeshFace {
  public:
    MeshFace();
    MeshFace(size_t vrtx1, size_t vrtx2, size_t vrtx3);
  public:
    // These are vertex indicies
    size_t fVert[3];
};
//-------------------------------------------------------------------//
class Mesh {
    friend class GLMesh; // GL buffer will need direct access
  public:
    /**
     * Mesh is transfered into the integer domain for faster operations.
     */
    Mesh(unsigned long long mm_to_int = 1000);

    /**
     * This function will create a new Mesh class, read file and return a pointer to it
     * Don't forget to free it, once it's no longer needed
     */
    static Mesh* LoadSTL(const QString& filename);


    float minX() const { return (float) fMinX / fConv; }
    float maxX() const { return (float) fMaxX / fConv; }
    float minY() const { return (float) fMinY / fConv; }
    float maxY() const { return (float) fMaxY / fConv; }
    float minZ() const { return (float) fMinZ / fConv; }
    float maxZ() const { return (float) fMaxZ / fConv; }

    //--------- We probably want to be Qt-agnostic from here...
    /**
     * These functions will load STL file in BIN and ASCII modes,
     * Will return false upon io error
     */
    bool LoadBinarySTL(std::string filename);
    bool LoadTextSTL(std::string filename);

    /**
     * This function will set problematic flag for all problematic faces
     * It will return the total number of problematic faces in the mesh
     */
    size_t CalculateProblematicFaces();

  private:
    /**
     * This function will return index of the face inserted
     */
    size_t  AddFace(MeshVertex &vrtx0, MeshVertex &vrtx1, MeshVertex &vrtx2);
    /**
     * This function will return the index of the vertex, if the vertex is already in vertices map
     * If the vertex is not in array it will pick up next available index, add vertex and return the resulting
     * index
     */
    size_t AddVertex(MeshVertex & vert);

  private:
    unsigned long long fConv;
    // We don't keep Mesh class for visualization,
    // Only for loading into GLMesh
    std::unordered_map <size_t, MeshFace> fFaces;
    std::size_t fNavFace;

    std::unordered_map <size_t, MeshEdge> fEdges;
    std::size_t fNavEdge;

    std::unordered_map <size_t, MeshVertex> fVertices;
    // Reverse map is only used in AddVertex function to check if Vertex already exist or not...
    std::unordered_map <MeshVertex, size_t, MeshVertex_hash> fVertReverse;
    std::size_t fNavVertex;
    
    // Bounding box
    long long fMaxX, fMaxY, fMaxZ, fMinX, fMinY, fMinZ;
};

#endif // _MESH_H
