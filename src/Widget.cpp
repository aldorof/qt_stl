#include <QMouseEvent>

#include <cmath> // for sqrt and pow

#include "Widget.h"
#include "GLMesh.h"
#include "Mesh.h"

//-------------------------------------------------------------------//
Widget::Widget(QWidget *parent): QOpenGLWidget(parent), 
        fMesh(NULL), fScale(1), fTilt(90), fYaw(0) {
    
}
//-------------------------------------------------------------------//
Widget::~Widget() {
    if (fMesh != NULL)
        delete fMesh;
}
//-------------------------------------------------------------------//
void Widget::LoadMesh(Mesh* m) {
    fMesh = new GLMesh(m);
    fCenter = QVector3D(
                    m->minX() + m->maxX(),
                    m->minY() + m->maxY(),
                    m->minZ() + m->maxZ()) / 2;
    fScale = 2 / std::sqrt(
                std::pow(m->maxX() - m->minX(), 2) +
                std::pow(m->maxY() - m->minY(), 2) +
                std::pow(m->maxZ() - m->minZ(), 2));
    update();
    delete m;
    m = NULL;
}
//-------------------------------------------------------------------//
void Widget::initializeGL() {
    fMeshShader.addShaderFromSourceFile(QOpenGLShader::Vertex,   ":/resources/glsl/mesh.vert");
    fMeshShader.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/glsl/mesh.frag");
    fMeshShader.link();

    initializeOpenGLFunctions();

    // Blue background...
    glClearColor(0.25, 0.45, 0.65, 1.0);
    glEnable(GL_DEPTH_TEST);
}
//-------------------------------------------------------------------//
void Widget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (fMesh) {
        DrawMesh();
    }
}
//-------------------------------------------------------------------//
void Widget::DrawMesh() {

    fMeshShader.bind();
    // Load the transform and view matrices into the shader
    glUniformMatrix4fv(
                fMeshShader.uniformLocation("transform_matrix"),
                1, GL_FALSE, TransformMatrix().data());
    glUniformMatrix4fv(
                fMeshShader.uniformLocation("view_matrix"),
                1, GL_FALSE, ViewMatrix().data());
    // Find and enable the attribute location for vertex position
    const GLuint vp = fMeshShader.attributeLocation("vertex_position");
    glEnableVertexAttribArray(vp);
    // Then draw the mesh with that vertex position
    fMesh->fVert.bind();
    fMesh->fIndx.bind();
    glVertexAttribPointer(vp, 3, GL_FLOAT, false, 3*sizeof(float), NULL);
    glDrawElements(GL_TRIANGLES, fMesh->fIndx.size() / sizeof(uint32_t), GL_UNSIGNED_INT, NULL);
    fMesh->fVert.release();
    fMesh->fIndx.release();
    // Clean up state machine
    glDisableVertexAttribArray(vp);
    fMeshShader.release();
}
//-------------------------------------------------------------------//
QMatrix4x4 Widget::TransformMatrix() const {

    QMatrix4x4 m;
    m.rotate(fTilt, QVector3D(1, 0, 0));
    m.rotate(fYaw,  QVector3D(0, 0, 1));
    m.scale(-fScale);
    m.translate(-fCenter);
    return m;
}
//-------------------------------------------------------------------//
QMatrix4x4 Widget::ViewMatrix() const {
    QMatrix4x4 m;
    if ( width() > height() ) {
        m.scale(height() / float(width()), 1, 0.5);
    } else {
        m.scale(1, width() / float(height()), 0.5);
    }
    return m;
}
//-------------------------------------------------------------------//
void Widget::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        fMousePos = event->pos();
        setCursor(Qt::ClosedHandCursor);
    }
}
//-------------------------------------------------------------------//
void Widget::mouseReleaseEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        unsetCursor();
    }
}
//-------------------------------------------------------------------//
void Widget::mouseMoveEvent(QMouseEvent* event) {
    if (event->buttons() & Qt::LeftButton) {
        auto p = event->pos();
        auto d = p - fMousePos;
        fYaw = fmod(fYaw + d.x(), 360);
        fTilt = fmax(0, fmin(180, fTilt - d.y()));
        fMousePos = p;
        update();
    }
}
//-------------------------------------------------------------------//

