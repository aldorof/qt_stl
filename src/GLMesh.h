#ifndef _GLMESH_H
#define _GLMESH_H

#include <QOpenGLBuffer>

class Mesh;
class GLMesh {
    friend class Widget; // Will need access to GLBuffers
  public:
    GLMesh(const Mesh* const mesh);
  private:
    QOpenGLBuffer fVert;
    QOpenGLBuffer fIndx;
};
#endif // _GLMESH_H
