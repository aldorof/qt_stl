#ifndef _LOADER_H
#define _LOADER_H

#include <QThread>

#include "Mesh.h"

class Loader : public QThread {
    Q_OBJECT
  public:
    explicit Loader(QObject* parent, const QString& filename);
    void run();

  signals:
    void got_mesh(Mesh* m);

  private:
    const QString fFileName;

};
#endif // _LOADER_H
